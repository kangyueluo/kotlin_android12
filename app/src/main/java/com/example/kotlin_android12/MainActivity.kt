package com.example.kotlin_android12

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.transition.Slide
import android.util.Log
import android.view.ViewParent
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    val TAG ="MainActivity"
    lateinit var  pager : ViewPager
    val images : IntArray = intArrayOf(R.drawable.img_1,R.drawable.img_2,R.drawable.img_3,R.drawable.img_4,R.drawable.img_5,R.drawable.img_6)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView(){
        val adapter : PagerAdapter = SliderPagerAdapter(application,images)
        pager = findViewById(R.id.pager)
        pager.adapter = adapter

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
               Log.d(TAG,"on page scroll state changed "+state)
            }

            override fun onPageScrolled(posistion: Int, posistionOffset: Float, posistionOffsetPixels: Int) {
                val currentPosition : Int = posistion +1

                val pageCountTextView : TextView = findViewById(R.id.pageCountTextView) as TextView
                pageCountTextView.setText(currentPosition.toString() +"/"+images.size)

            }

            override fun onPageSelected(position: Int) {
                Log.d(TAG,"on page selected "+position)
            }

        })

    }
}
